This repo has the source code for the package TOM. 

The package TOM has been tested in the environment with Ubuntu 16.04.3 LTS and R 3.4.3

The compiled package TOM can be found in the link below:

https://bitbucket.org/vtshen/rpackages/src/master/

The vignette can be viewed by 

```r
browseVignettes(package="TOM")
```

---

Useful links for building R packages:

- R packages by Hadley Wickham http://r-pkgs.had.co.nz/
