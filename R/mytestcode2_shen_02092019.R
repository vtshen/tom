#' sim.xy to generate the data in the simulation section
#'
#' @param signal2noise_ratio numeric signal-to-noise ratio
#' @param m numeric data size of intermediate measurements and final measurements, assuming they are the same.
#' @param beta0_true numeric, intercept in the linear regression model
#' @param beta1_true numeric, slope in the linear regression model
#' @param sigma2_true numeric, variance of the error term in the linear regression model
#' @param alpha_true numeric, mean of the normal distribution of the variable X
#' @param delta2_true numeric, variance of the normal distribution of the variable X
#'
#' @return Return a list with the following components:
#' \item{x}{numeric, observed intermediate measurements}
#' \item{y}{numeric, observed final measurements}
#' \item{xfull}{numeric, all x, including observed and missing intermediate measurements}
#' \item{yfull}{numeric, all y, including observed and missing final measurements}
#' \item{xtrue}{numeric, all x, including observed and missing final measurements, and the x in the simulated paired (x,y)}
#' \item{ytrue}{numeric, all y, including observed and missing final measurements and the y in the simulated paired (x,y)}
#' \item{k_signal2noise_ratio}{numeric, used together with the signal-to-noise ratio, to compute the variance of error}


sim.xy = function(signal2noise_ratio=3, m, beta0_true, beta1_true, sigma2_true, alpha_true, delta2_true){
  if (0) {
    beta0_true = 30
    beta1_true = 5
    sigma2_true = 1
    alpha_true = 5
    delta2_true = 0.5^2
    signal2noise_ratio=3

    set.seed(02282018)
    m=20
  }

  n = m + m + 5
  x_true = rnorm(n, mean = alpha_true, sd = (delta2_true^0.5))
  #y_true = beta0_true + beta1_true * x + rnorm(n, mean = 0, sd = (sigma2_true^0.5))
  y_pure = beta0_true + beta1_true * x_true

  if (1) {
    convert = function(y_true, sd, signal2noise_ratio){
      # keep the SN ratio = 3:1
      noise = rnorm(length(y_true),mean = 0,sd = sd) # generate standard normal errors
      k = sqrt(var(y_true)/(signal2noise_ratio*var(noise)))
      y = y_true + k*noise
      return(enlist(y, k))
    }
    # https://stats.stackexchange.com/questions/31158/how-to-simulate-signal-noise-ratio
    tmp = convert(y_true = y_pure, sd=sigma2_true^0.5, signal2noise_ratio = signal2noise_ratio)
  } else {
    # an alternative approach, but SNR is not fixed
    convert = function(y_true, signal2noise_ratio, sd = 1){
      k2 = var(y_true)/(signal2noise_ratio)
      k = sqrt(k2)
      noise = rnorm(length(y_true),mean = 0,sd = k) # generate standard normal errors
      y = y_true + noise
      return(enlist(y, k))
    }
    tmp = convert(y_true = y_pure, signal2noise_ratio = signal2noise_ratio)
  }

  y_true = tmp$y
  k_signal2noise_ratio = tmp$k

  ## partition data for missing assignment
  x = x_true[1:m]
  y = y_true[(m+1):(2*m)]
  x_pair = x_true[(2*m+1):(2*m+5)]
  y_pair = y_true[(2*m+1):(2*m+5)]

  xfull = x_true[1:(2*m)]
  yfull = y_true[1:(2*m)]
  out = enlist(x,y, xfull, yfull, x_true, y_true, k_signal2noise_ratio)
  return (out)
}

#' testEMcodemrange function provided by Profs. Steiner and Mackay,
#' used to run simulation study 1 in order to study the effect of sample size on the method performance
#' modified a bit by Shen
#'
#' @param nsim numeric, number of repeats for each simulation run
#' @param mrange numeric vector, vector of sample size
#' @param rho_type character value, corresponds to the rho seetings in the simulatio study 2 in the response letter
#'
#' @return Return a list with the following components:
#' \item{outlist}{resEM, res0, resLR, beta0_true, beta1_true, sigma2_true_path, alpha_true, delta2_true, signal2noise_ratio}


testEMcodemrange = function(nsim,mrange, rho_type=NULL)  {
#determine the performance of EM and set sigma=0 approaches
#consider a range of sample sizes (given by mrange)
#consider the model Y = beta0 + beta1*X + E, where X~(alpha,delta), E~G(0,sigma), R and E independent
# m = number of marginal observations for each of X and Y

#set the model parameters equal to that used in Shen (2018)
beta0_true <- 30
beta1_true <- 5
alpha_true <- 5
delta2_true <- 0.5^2
sigma2_true <- 1 # 1.2^2
signal2noise_ratio = 3

# set the model parameters equal to the 2nd version of the response letter, 09022019
# alpha = 5
# delta = 0.5

if (!is.null(rho_type)) {
  rho_vector = c(0.99, 0.9, 0.75, 0.5, 0.25) # settings used in the response letter
  beta1_vector = 2*2.77*rho_vector
  beta0_vector = 55 - beta1_vector
  sigma2_vector = 2.77^2 - 0.25*(beta1_vector^2)
  SNR_vector = 1/(1-rho_vector^2)

  # use the rho_type as the index
  rho_true = rho_vector[rho_type]; beta0_true <- beta0_vector[rho_type]; beta1_true <- beta1_vector[rho_type]
  alpha_true <- 5; delta2_true <- 0.5^2;
  sigma2_true <- sigma2_vector[rho_type]; signal2noise_ratio = SNR_vector[rho_type]
}



if (0) {
  rm(list=ls())
  source("/home/shen/R/Rpackages/TOM/R/EM.R")
  source("/home/shen/R/Rpackages/TOM/R/logL.R")
  source("/home/shen/R/Rpackages/TOM/R/plots.R")
  source("/home/shen/R/Rpackages/TOM/R/common.R")
  #library(TOM)

  beta0_true <- 30
  beta1_true <- 5
  alpha_true <- 5
  delta2_true <- 0.5^2
  sigma2_true <- 1 # 1.2^2
  jj=1
  ii=1
  nsim=10
  mrange=c(5000)
  m <- mrange[jj]
  x = rnorm(m, mean = alpha_true, sd = sqrt(delta2_true))
  y = rnorm(m, mean = beta0_true+beta1_true*alpha_true, sd = sqrt(beta1_true^2*delta2_true+sigma2_true))

  # test EM
  beta0_init = mean(y); beta1_init = 1; alpha_init = mean(x); sigma2_init=var(y); delta2_init = var(x);
  tol = 1e-3; niter = 1e3; type = "2segments"; plot = FALSE ;trace = FALSE; lowerLimit = min(y); upperLimit = max(y)
  base_dir = "~/";alpha_sig = 0.05
  x_pair = NULL; y_pair = NULL

  #fit the model via the EM algorithm - assuming mising data
  out_2seg = EM (x, y, beta0_init = mean(y), beta1_init = 1, alpha_init = mean(x), sigma2_init=var(y), delta2_init = var(x),
                 tol = 1e-3, niter = 1e4, type = "2segments", plot = FALSE ,trace = TRUE, lowerLimit = min(y), upperLimit = max(y))
  out_2seg$beta1

  out_test = testEMcodemrange (nsim=5, mrange=50)
  out_test = testEMcodemrange (nsim=5, mrange=50, rho_type=2) # corresponds to the second version of the response letter
  out_test
}
out_list = vector(mode="list", length = length(mrange))

for (jj in 1:length(mrange))  {
  m <- mrange[jj]
  resLR_naive = resLR = resEM <- matrix(0, nsim, 5)   #for storing all the model parameter estimates from EM approach
  resLR_naiveBVN = resLRBVN = resEMBVN = res0 <- matrix(0, nsim, 5)   #for storing all the model parameter estimates from set sigma=0 approach
  sigma2_true_path = rep(0, nsim)

  for (ii in 1:nsim)  {
    #generate the random data
    #x = rnorm(m, mean = alpha_true, sd = sqrt(delta2_true))
    #y = rnorm(m, mean = beta0_true+beta1_true*alpha_true, sd = sqrt(beta1_true^2*delta2_true+sigma2_true))

    sim_ = sim.xy(signal2noise_ratio, m, beta0_true, beta1_true, sigma2_true, alpha_true, delta2_true)
    x = sim_$x
    y = sim_$y
    xfull = sim_$xfull
    yfull = sim_$yfull
    k_signal2noise_ratio = sim_$k_signal2noise_ratio
    sigma2_true_path[ii] = sigma2_true*k_signal2noise_ratio*k_signal2noise_ratio # not used in Tables

    lmfit <- lm(yfull~xfull)
    resLR[ii,] = c(coef(lmfit)[1], coef(lmfit)[2], summary(lmfit)$sigma, mean(xfull), sd(xfull))

    lmfit_naive <- lm(y~x)
    resLR_naive[ii,] = c(coef(lmfit_naive)[1], abs(coef(lmfit_naive)[2]), summary(lmfit_naive)$sigma, mean(x), sd(x))

    #fit the model via the EM algorithm - assuming mising data
    out_2seg = EM (x, y, beta0_init = mean(y), beta1_init = 1, alpha_init = mean(x), sigma2_init=var(y), delta2_init = var(x),
         tol = 1e-3, niter = 1e3, type = "2segments", plot = FALSE ,trace = FALSE, lowerLimit = min(y), upperLimit = max(y))

    #save results from the EM approach. Note that we only consider the absolute value of the slope estimate
    resEM[ii,] <- c(out_2seg$beta0, abs(out_2seg$beta1), sqrt(out_2seg$sigma2), out_2seg$alpha, sqrt(out_2seg$delta2))

    #for comparison also fit the model with the additional assumption that sigma=0
    # res0[ii,] <- c(mean(y)-(mean(x)*sd(y))/sd(x), sd(y)/sd(x), 0, mean(x), sd(x))
    #for comparison also fit the model with the marginal approach in the 2nd version of response letter
    res0[ii,] = c(mean(x), sd(x), mean(y), sd(y), 1) # c("mux", "sdx", "muy", "sdy", "rho") # copy from their Rcode

    # corresponding BVN values for parameters
    #translate results into BVN parameterization
    muxLR <- mean(xfull)
    muyLR <- coef(lmfit)[1] + coef(lmfit)[2]*mean(xfull)
    sdxLR <- sd(xfull)
    sdyLR <- sqrt((coef(lmfit)[2])^2*var(xfull)+(summary(lmfit)$sigma)^2)
    rhoLR <- abs(coef(lmfit)[2]*sd(xfull))/sqrt((coef(lmfit)[2]*sd(xfull))^2+(summary(lmfit)$sigma)^2)
    resLRBVN[ii,] <- c(muxLR, sdxLR, muyLR, sdyLR, rhoLR)

    muxLR_naive <- mean(x)
    muyLR_naive <- coef(lmfit_naive)[1] + coef(lmfit_naive)[2]*mean(x)
    sdxLR_naive <- sd(x)
    sdyLR_naive <- sqrt((coef(lmfit_naive)[2])^2*var(x)+(summary(lmfit_naive)$sigma)^2)
    rhoLR_naive <- abs(coef(lmfit_naive)[2]*sd(x))/sqrt((coef(lmfit_naive)[2]*sd(x))^2+(summary(lmfit_naive)$sigma)^2)
    resLR_naiveBVN[ii,] <- c(muxLR_naive, sdxLR_naive, muyLR_naive, sdyLR_naive, rhoLR_naive)

    muxEM <- out_2seg$alpha
    muyEM <- out_2seg$beta0 + abs(out_2seg$beta1)*out_2seg$alpha
    sdxEM <- sqrt(out_2seg$delta2)
    sdyEM <- sqrt(out_2seg$beta1^2*out_2seg$delta2 + out_2seg$sigma2)
    rhoEM <- abs(out_2seg$beta1)*sqrt(out_2seg$delta2)/sqrt(out_2seg$beta1^2*out_2seg$delta2 + out_2seg$sigma2)
    resEMBVN[ii,] <- c(muxEM, sdxEM, muyEM, sdyEM, rhoEM)
    }  #end simulation for loop

  print('true values beta0, beta1, sigma, alpha, delta')
  print(c(beta0_true, beta1_true,sqrt(sigma2_true),alpha_true,sqrt(delta2_true)))
  print('summary results for model parameter estimates (first simple mean and sd then same for EM approach)')
  print(paste('sample size = ',m))
  print(apply(res0, 2, mean))
#  print(apply(res0, 2, mean)-c(beta0_true,beta1_true,sqrt(sigma2_true),alpha_true,sqrt(sigma2_true)))
  print(apply(res0, 2, sd))
  print(apply(resEM, 2, mean))
#  print(apply(resEM, 2, mean)-c(beta0_true,beta1_true,sqrt(sigma2_true),alpha_true,sqrt(sigma2_true)))
  print(apply(resEM, 2, sd))

  # modification from Shen
  name = c("beta0", "beta1_abs", "sigma", "alpha", "delta")
  colnames(resLR_naiveBVN) = colnames(resLRBVN) = colnames(resEMBVN) = colnames(res0) = c("mux", "sdx", "muy", "sdy", "rho")
  colnames(resLR_naive) = colnames(resLR) = colnames(resEM) =  name

  out_list[[jj]] = enlist(resEM, res0, resLR, resLR_naive, resEMBVN, resLRBVN, resLR_naiveBVN, beta0_true, beta1_true, sigma2_true_path, alpha_true, delta2_true, signal2noise_ratio)
  }#end top for loop

  return (out_list)
} #end function

#' testEMcodesigmarange function provided by Profs. Steiner and Mackay,
#' used to run simulation study 2 in order to study the effect of signal-to-noise ratio on the method performance
#' modified a bit by Shen
#'
#' @param nsim numeric, number of repeats for each simulation run
#' @param signal2noise_ratio_range numeric vector, vector of signal-to-noise ratio
#'
#' @return Return a list with the following components:
#' \item{outlist}{resEM, res0, resLR, beta0_true, beta1_true, sigma2_true_path, alpha_true, delta2_true, signal2noise_ratio}


testEMcodesigmarange = function(nsim,signal2noise_ratio_range)  {
#determine the performance of EM and set sigma=0 approaches
#consider a range of sigma value (given by sigmarange)
#consider the model Y = beta0 + beta1*X + E, where X~G(alpha,delta), E~G(0,sigma), R and E independent
# m = number of marginal observations for each of X and Y

if (0) {
  ii = 1
  jj=1
  signal2noise_ratio_range = c(0.5, 1)
  nsim = 10
}

#set the model parameters equal to that used in Shen (2018), except sigma - since we plan to change the value
beta0_true <- 30
beta1_true <- 5
alpha_true <- 5
delta2_true <- 0.5^2
sigma2_true = 1
m <- 50  #sample size
out_list = vector(mode="list", length = length(signal2noise_ratio_range))

for (jj in 1:length(signal2noise_ratio_range))  {
  #sigma2_true <- sigmarange[jj]^2    #note we store the variance
  resLR = resEM <- matrix(0, nsim, 5)   #for storing all the model parameter estimates from EM approach
  res0 <- matrix(0, nsim, 5)   #for storing all the model parameter estimates from set sigma=0 approach
  signal2noise_ratio = signal2noise_ratio_range[jj]
  sigma2_true_path = rep(0, nsim)

  for (ii in 1:nsim)  {
    #generate the random data
    # x = rnorm(m, mean = alpha_true, sd = sqrt(delta2_true))
    # y = rnorm(m, mean = beta0_true+beta1_true*alpha_true, sd = sqrt(beta1_true^2*delta2_true+sigma2_true))
    sim_ = sim.xy(signal2noise_ratio, m, beta0_true, beta1_true, sigma2_true, alpha_true, delta2_true)
    x = sim_$x
    y = sim_$y
    xfull = sim_$xfull
    yfull = sim_$yfull
    k_signal2noise_ratio = sim_$k_signal2noise_ratio
    sigma2_true_path[ii] = sigma2_true*k_signal2noise_ratio*k_signal2noise_ratio # not used in the Tables in the paper

    lmfit <- lm(yfull~xfull)
    coef(lmfit)[2]
    resLR[ii,] = c(coef(lmfit)[1], coef(lmfit)[2], summary(lmfit)$sigma, mean(xfull), sd(xfull))

    #fit the model via the EM algorithm - assuming mising data
    out_2seg = EM (x, y, beta0_init = mean(y), beta1_init = 1, alpha_init = mean(x), sigma2_init=var(y), delta2_init = var(x),
         tol = 1e-3, niter = 1e3, type = "2segments", plot = FALSE ,trace = FALSE, lowerLimit = min(y), upperLimit = max(y))

    #save results from the EM approach. Note that we only consider the absolute value of the slope estimate
    resEM[ii,] <- c(out_2seg$beta0, abs(out_2seg$beta1), sqrt(out_2seg$sigma2), out_2seg$alpha, sqrt(out_2seg$delta2))

    #for comparison also fit the model with the additional assumption that sigma=0
    res0[ii,] <- c(mean(y)-(mean(x)*sd(y))/sd(x), sd(y)/sd(x), 0, mean(x), sd(x))
    }  #end simulation for loop

  print('true values beta0, beta1, alpha, delta, sample size')
  print(c(beta0_true, beta1_true,alpha_true,sqrt(delta2_true),m))
  print('summary results for model parameter estimates (first simple mean and sd then same for EM approach)')
  print(paste('sigma = ',sqrt(sigma2_true)))
  print(apply(res0, 2, mean))
#  print(apply(res0, 2, mean)-c(beta0_true,beta1_true,sqrt(sigma2_true),alpha_true,sqrt(sigma2_true)))
  print(apply(res0, 2, sd))
  print(apply(resEM, 2, mean))
#  print(apply(resEM, 2, mean)-c(beta0_true,beta1_true,sqrt(sigma2_true),alpha_true,sqrt(sigma2_true)))
  print(apply(resEM, 2, sd))

  name = c("beta0", "beta1_abs", "sigma", "alpha", "delta")
  colnames(resLR) = colnames(resEM) = colnames(res0) = name

  out_list[[jj]] = enlist(resEM, res0, resLR, beta0_true, beta1_true, sigma2_true_path, alpha_true, delta2_true, signal2noise_ratio)

  }  #end top for loop
  return (out_list)

} #end function

if (0) {
  # Dec 10, 2018
  # test code
  library(TOM)
  folder = "/home/shen/Documents/VT/LabDrDeng/ProjectMultistageTolerance/comments_reviewer/rejoinder/"
  nsim = 1000
  mrange = c(20, 50, 100, 1000, 5000) #, 100, 1000, 5000)

  res_size = testEMcodemrange (nsim,mrange)
  res_size
  save(nsim, mrange, res_size, file=paste(folder, "tom_simulation_results_varying_size_Feb152019.RData", sep=''))

  # the second argument
  nsim = 1000
  signal2noise_ratio_range = c(0.5, 1, 3, 6, 10)

  res_snr = testEMcodesigmarange (nsim,signal2noise_ratio_range)
  res_snr
  save(nsim, signal2noise_ratio_range, res_snr, file=paste(folder, "tom_simulation_results_varying_snr_Feb152019.RData", sep=""))

  # Feb 10, 2019
  # 2nd version response letter
  rho_type_vector = c(1,2,3,4,5) # index vector
  nsim = 1000 #1000
  mrange = 50

  folder = "/home/shen/Documents/VT/LabDrDeng/ProjectMultistageTolerance/comments_reviewer/rejoinder_after_review/rmd_files/"
  res_rho = vector(mode="list", length = length(rho_type_vector))
  for (k in 1:length(rho_type_vector)) {
    rho_type_index = k
    out_test = testEMcodemrange (nsim=nsim, mrange=mrange, rho_type=rho_type_index) # corresponds to the second version of the response letter
    res_rho[[k]] = out_test
  }
  save(nsim, mrange, res_rho, file=paste(folder, "tom_simulation_results_varying_rho_Feb152019.RData", sep=''))

}
